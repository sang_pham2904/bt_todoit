export const ToDoListPrimaryTheme = {
    bgColor : '#fff',
    color : '#343a40',
    borderButton: '1px solid #343a40',
    boderRadiusButton: 'none',
    hoverTextColor : "#343a40",
    hoverBgColor : "#fff",
    borderColor : "#343a40"
}