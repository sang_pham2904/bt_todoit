import styled,{ThemeProvider} from 'styled-components'
import React, { Component } from 'react'

const configTheme = {
    background:'#000',
    color:'#fff',
    fontSize: '15px'
}
const configLightTheme = {
    background :'#6633FF',
    color:'#fff',
    fontSize: '20px'

}
export default class DemoThemes extends Component {
    state = {
        currentTheme: configTheme
    }
    handleChangeTheme =(e) => {
     this.setState({
        currentTheme : e.target.value == '1' ? configTheme : configLightTheme
     })
    }
  render() {

    let DivStyle = styled.div`
        color: ${props => props.theme.color};
        padding : 5%;
        background-color: ${props => props.theme.background};
        font-size: ${props => props.theme.fontSize}
    `
    return (
        <ThemeProvider theme={this.state.currentTheme}>
        <DivStyle>Hello</DivStyle>
        <select onChange={this.handleChangeTheme}>
            <option value ='1'>DarkTheme</option>
            <option value ='2'>LightTheme</option>
        </select>

    </ThemeProvider>
    )
  }
}
