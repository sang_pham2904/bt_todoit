import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Container } from "../../Components/Container";
import { ToDoListDarkTheme } from "../../Themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../../Themes/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../../Themes/ToDoListPrimaryTheme";
import { Dropdown } from "../../Components/Dropdown";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../../Components/Heading";
import { Input, Label, TextField } from "../../Components/TextField";
import { Button } from "../../Components/Button";
import { Table, Th, Thead, Tr } from "../../Components/Table";
import { connect } from "react-redux";
import { addTaskAction,changeThemeAction,deleteTaskAction,doneTaskAction, editTaskAction, updateAction } from "../../../redux/actions/ToDoListActions";
import { arrTheme } from "../../Themes/ThemeManager";
class ToDoList extends Component {
  state ={
    taskName : '',
    disabled : true
  }
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((item) => !item.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th>{task.taskName}</Th>
            <Th className="text-right">
              <Button onClick={()=>{
                this.setState({disabled: false}, () => {
                this.props.dispatch(editTaskAction(task))
                })
              }}>
                <i className="fa fa-edit"></i>
              </Button >
              <Button onClick={()=>{
                this.props.dispatch(doneTaskAction(task.id))
              }}>
                <i className="fa fa-check"></i>
              </Button>
              <Button onClick={() => {
               this.props.dispatch(deleteTaskAction(task.id))
              }}>
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskComplete = () => {
    return this.props.taskList
    .filter((item) => item.done)
    .map((task, index) => {
      return (
        <Tr key={index}>
          <Th>{task.taskName}</Th>
          <Th className="text-right">
            <Button onClick={() => {
               this.props.dispatch(deleteTaskAction(task.id))
              }}>
              <i className="fa fa-trash"></i>
            </Button>
          </Th>
        </Tr>
      );
    });
  }
  renderTheme = () => {
  return arrTheme.map((item,index)=>{
      return <option key={index} value ={item.id} >{item.name}</option>
   })
  }
  addTask = ()=>{
    // Lấy thông tin người dùng nhập vào từ input
    let {taskName} = this.state
    // Tạo ra 1 task obj
    let newTask = {
      id : Date.now(),
      taskName: taskName,
      done : false
    }   
    // đưa taskk obj lên redux thông qua dispatch
    this.props.dispatch(addTaskAction(newTask))
    // this.setState({
    //   taskName : ''
    // })
  }
  // componentWillReceiveProps(newProps){
  //   console.log(this.props);
  //   console.log(newProps);
  //   this.setState({
  //     taskName : newProps.taskEdit.taskName
  //   })
  // }

  // static getDerivedStateFromDrops(newProps, currentState){

  //   let newState = {...currentState, taskName: newProps.taskEdit.taskName}
  //   return newState
  // }

  render() {

    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown onChange={(e)=>{
            let {value} = e.target
            this.props.dispatch(changeThemeAction(value))
          }}>
            {this.renderTheme()}
          </Dropdown>
          <Heading2>To Do List</Heading2>
          {/* <Label>Task Name</Label>
            <Input></Input> */}
          <TextField label={"Task Name"} className="w-50" name = "taskName" onChange = {(e)=>{this.setState({taskName: e.target.value})}} onKeyUp = {(e) =>{
            if(e.key === "Enter"){
              this.addTask()
            }
          }} value = {this.state.taskName}></TextField>
          <Button  className="ml-2" onClick={this.addTask}>
            <i className="fa fa-plus"></i> Add Task
          </Button>
          {this.state.disabled ? <Button disabled className="ml-2" onClick={() => this.props.dispatch(updateAction(this.state.taskName, this.props.taskEdit.id))}>
            <i className="fa fa-upload"></i> Update Task
          </Button>:
          <Button className="ml-2" onClick={() =>{
            this.setState({
              disabled: true}, ()=> this.props.dispatch(updateAction(this.state.taskName, this.props.taskEdit.id)) )} }>
            <i className="fa fa-upload"></i> Update Task
          </Button>}
          
          <hr />
          <Heading2>Task To Do</Heading2>
          <Table>
            <Thead>
             {this.renderTaskToDo()}
            </Thead>
          </Table>
          <Heading2>Task Complete</Heading2>
          <Table>
            <Thead>
              {this.renderTaskComplete()}
            </Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }

  componentDidUpdate(preProps, preState){
    if(preProps.taskEdit.id !== this.props.taskEdit.id ){

      this.setState({
        taskName: this.props.taskEdit.taskName
      })
    }
  }
}
let mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit : state.ToDoListReducer.taskEdit
  };
};
export default connect(mapStateToProps)(ToDoList);
