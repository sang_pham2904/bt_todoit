import React, { Component } from 'react'
import { Button, SmallButton } from '../Components/Button'
import { StyledLinkk } from '../Components/Link'
import { TextField } from '../Components/TextField'

export default class DemoJSS extends Component {
  render() {
    return (
      <div>
        <Button className='button_styled' bgPrimary style={{color : "red"}}>Click</Button>
        {/* <SmallButton>Hello</SmallButton>
        <StyledLinkk>Hi</StyledLinkk> */}
        <TextField inputColor = "yellow"/>
        </div>
    )
  }
}
