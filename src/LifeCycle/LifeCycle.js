import React, { Component } from 'react'
import ChildComponent from './ChildComponent'

export default class LifeCycle extends Component {
    // static getDer
    state ={
      number : 1,
  }
    shouldComponentUpdate(nextProps, nextState){
      console.log(nextState.number);
      // return false
      if(nextState.number === 5 ){
        return false
      }  else{
        return true
      }
    }
    


  render() {
    return (
      <div>
        <h1>Parent Component</h1>
        <span>{this.state.number}</span>
        <button className='btn btn-success' onClick={()=>{
            this.setState({
                number : this.state.number +1
            })
        }}>click</button>
       {this.state.number < 2 &&<ChildComponent/>}
      </div>
    )
  }
  componentDidMount(){
    console.log('componentDidMount');
  }
}
