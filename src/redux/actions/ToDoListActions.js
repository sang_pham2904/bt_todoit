import { add_task, change_theme, delete_task, done_task, edit_task, update } from "../types/ToDoListType";


export const addTaskAction = (newTask) => ({
  type: add_task,
  newTask
})

export const changeThemeAction = (payload) => ({
  type : change_theme,
  payload
})
export const doneTaskAction = (payload) => ({
  type : done_task,
  payload
})
export const deleteTaskAction = (payload) => ({
  type : delete_task,
  payload
})
export const editTaskAction = (payload) => ({
  type: edit_task,
  payload
})
export const updateAction = (value,id) => ({
  type: update,
  payload : {value,id }
})

