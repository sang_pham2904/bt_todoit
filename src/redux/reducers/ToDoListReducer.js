import { arrTheme } from "../../JSS_Styled_Component/Themes/ThemeManager"
import { ToDoListDarkTheme } from "../../JSS_Styled_Component/Themes/ToDoListDarkTheme"
import { ToDoListLightTheme } from "../../JSS_Styled_Component/Themes/ToDoListLightTheme"
import { ToDoListPrimaryTheme } from "../../JSS_Styled_Component/Themes/ToDoListPrimaryTheme"
import { add_task, change_theme, delete_task, done_task, edit_task, update } from "../types/ToDoListType"

const initialState = {
    themeToDoList : ToDoListDarkTheme,
    taskList : [
        {id : 1 ,taskName: 'task 1', done : true},
        {id : 2 ,taskName: 'task 2', done : false},
        {id : 3 ,taskName: 'task 3', done : true},
        {id : 4 ,taskName: 'task 4', done : false},
    ],
    taskEdit : {id : -1 ,taskName: '', done : false},
}

export default (state = initialState, action) => {
  switch (action.type) {

  case add_task:{
    if(action.newTask.taskName.trim() === ''){
        alert('Vui lòng nhập thông tin')
        return{...state}
    }
    let cloneTaskList = [...state.taskList]
    let index = cloneTaskList.findIndex(item=>{
        return item.taskName == action.newTask.taskName
    })
    if(index !== -1){
        alert('Task đã tồn tại')
        return {...state}
    }else{
        cloneTaskList.push(action.newTask)
        return {...state,taskList : cloneTaskList}
    }
  }

  case change_theme:{
    let theme = arrTheme.find((item) => {
        return item.id == action.payload
    })
    if(theme){
        console.log('theme.theme: ', theme.theme);
        return{...state, themeToDoList : theme.theme}
    }
  }
  case done_task:{
    let cloneTaskList = [...state.taskList]
    let index = cloneTaskList.findIndex(item =>{
        return item.id == action.payload
    })
    if(index !== -1){
        cloneTaskList[index].done = true
        return {...state,taskList : cloneTaskList}
    }
  }
  case delete_task:{
    console.log(action.payload);
    let cloneTaskList = [...state.taskList]
    let index = cloneTaskList.findIndex(item =>{
        return item.id == action.payload
    })
    if(index !== -1){
        cloneTaskList.splice(index,1)
        return {...state,taskList : cloneTaskList}
    }
  }

  case edit_task :{
    return {...state, taskEdit : action.payload}
  }

  case update : {
    console.log(action.payload);
    let cloneTaskList = [...state.taskList]
    let cloneTaskEdit = {...state.taskEdit}
    let index = cloneTaskList.findIndex((item) =>{
      return item.id == action.payload.id
    })
    if (index !== -1){
      cloneTaskList[index].taskName = action.payload.value
      cloneTaskEdit = {id : -1 ,taskName: '', done : false}
    }
    return {...state,taskList : cloneTaskList, taskEdit : cloneTaskEdit}
  }
  default:
    return state
  }
}
