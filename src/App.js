import logo from './logo.svg';
import './App.css';
import DemoJSS from './JSS_Styled_Component/DemoJSS/DemoJSS';
import LifeCycle from './LifeCycle/LifeCycle';
import DemoThemes from './JSS_Styled_Component/Themes/DemoThemes';
import ToDoList from './JSS_Styled_Component/BaiTapStyledComponent/ToDoList/ToDoList';



function App() {
  return (
    <div>
      {/* <DemoJSS/> */}
      {/* <LifeCycle/> */}
      {/* <DemoThemes/> */}
      <ToDoList/>
    </div>
  );
}

export default App;
